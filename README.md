# Curso .NET 🚀

Bem-vindo ao repositório oficial do curso de .NET! Aqui você encontrará todo o material necessário para se tornar um desenvolvedor .NET capacitado e pronto para enfrentar desafios emocionantes.

## Programação das Aulas 📅

### ✅ Aula 1: Introdução e Setup do Ambiente (23/01/24)

- Introdução ao curso, apresentação dos objetivos, metodologia e avaliação. Setup do ambiente de desenvolvimento com Visual Studio, .NET Framework e C#. Revisão de lógica de programação com algoritmos, estruturas de dados, fluxo de controle e funções.

    [Material da aula](./aulas/01/01.md)

### ✅ Aula 2: Lógica de Programação e Algorítimos (30/01/24)

- Revisão de lógica de programação com algoritmos, estruturas de dados, fluxo de controle e funções

    [Material da aula](./aulas/02/02.md)

### ✅ Aula 3: Programação orientada a objetos básica (01/02/24)

- Revisão de programação orientada a objetos básica com C#. Conceitos de Classe, instância, Métodos, Atributos, Construtor, encapsulamento, Herança, polimorfismo e interfaces.

    [Material da aula](./aulas/03/03.md)

### ✅ Aula 4: Programação orientada a objetos II (06/02/24)

- Conceito de classes abstratas, generics, tipos genéricos, métodos genéricos e coleções genéricas. Exemplos e exercícios com generics em C#

    [Material da aula](./aulas/04/04.md)

### ✅ Aula 5: Programação orientada a objetos II (08/02/24)

- Conceitos e princípios SOLID, explicação sobre SRP-Princípio da Responsabilidade Única e conceitos básicos sobre Design Patterns.
Exemplos e exercícios em C#.

    [Material da aula](./aulas/05/05.md)

### ✅ Aula 6: Programação orientada a objetos II (15/02/24)

- Conceito de aberto e fechado, princípio de substituição de Liskov, princípio de inversão de dependência e injeção de dependência. Exemplos e exercícios com princípios SOLID e padrões de projeto em C#.

    [Material da aula](./aulas/06/06.md)

### ✅ Aula 7: Técnicas de programação I (20/02/24)

- Introdução ao .NET

    [Material da aula](./aulas/07/07.md)

### ✅ Aula 8: Técnicas de programação I (22/02/24)

- Conceito de lambdas, expressões lambda, delegates e funções anônimas. Exemplos e exercícios com lambdas em C#.

    [Material da aula](./aulas/08/08.md)

### ✅ Aula 9: Técnicas de programação I (27/02/24)

- Conceito de collections, listas, pilhas, filas, dicionários e conjuntos. Exemplos e exercícios com collections em C#.

    [Material da aula](./aulas/09/09.md)

### ✅ Aula 10: Técnicas de programação I (29/02/24)

- Conceito de linq, consulta de dados em memória, em arquivos, em bancos de dados e em XML. Exemplos e exercícios com linq em C#.

    [Material da aula](./aulas/10/10.md)

### ✅ Aula 11: Técnicas de programação I (05/03/24)

- Conceito de programação paralela, threads, processos, sincronização e comunicação. Exemplos e exercícios com programação paralela em C#.

    [Material da aula](./aulas/11/11.md)

### ✅ Aula 12: Técnicas de programação I (07/03/24)

- Conceito de programação assíncrona, async, await, callbacks e promises. Exemplos e exercícios com programação assíncrona em C#.

    [Material da aula](./aulas/12/12.md)

### ✅ Aula 13: Técnicas de programação I (12/03/24)

- Conceito de tasks, task factory, task scheduler, task parallel library e parallel linq. Exemplos e exercícios com tasks em C#.

    [Material da aula](./aulas/13/13.md)

### ✅ Aula 14: Banco de dados SQL Server (14/03/24)

- Conceito de modelagem entidade relacionamento, diagrama entidade relacionamento, entidade, atributo, relacionamento, cardinalidade e grau. Exemplos e exercícios com modelagem entidade relacionamento.

    [Material da aula](./aulas/14/14.md)

### ✅ Aula 15: Banco de dados SQL Server (20/03/24)

- Conceito de modelo físico, esquema, tabela, coluna, chave primária, chave estrangeira, índice e restrição. Exemplos e exercícios com modelo físico e criação de tabelas em SQL Server.

    [Material da aula](./aulas/15/15.md)

### Aula 16: Banco de dados SQL Server (21/03/24)

- Conceito de normalização, formas normais, dependência funcional, anomalias e desnormalização. Exemplos e exercícios com normalização e desnormalização de tabelas em SQL Server.

    [Material da aula](./aulas/16/16.md)

### Aula 17: Banco de dados SQL Server (26/03/24)

- Conceito de queries simples, linguagem SQL, operadores, funções, cláusulas, seleção, projeção, ordenação, agrupamento e agregação. Exemplos e exercícios com queries simples em SQL Server.

    [Material da aula](./aulas/17/17.md)

### Aula 18: Banco de dados SQL Server (28/03/24)

- Conceito de queries complexas, junções, subqueries, uniões, interseções, diferenças, existência e inexistência. Exemplos e exercícios com queries complexas em SQL Server.

    [Material da aula](./aulas/18/18.md)

### Aula 19: Banco de dados SQL Server (02/04/24)

- Conceito de otimização, plano de execução, análise de desempenho, índices, estatísticas, particionamento e cache. Exemplos e exercícios com otimização de queries em SQL Server.

    [Material da aula](./aulas/19/19.md)
