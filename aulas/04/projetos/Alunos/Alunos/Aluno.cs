public class Aluno : Pessoa
{
    public ITurma Turma { get; set; }
    private List<Avaliacao> _avaliacoes { get; set; } = new List<Avaliacao>();


    public Aluno(string nome, int idade, ITurma turma) : base(nome, idade)
    {
        Turma = turma;
    }

    public void AdicionarAvaliacao(Avaliacao avaliacao)
    {
        if (avaliacao.Nota < 0 || avaliacao.Nota > 10)
            throw new Exception("Nota inválida");
        _avaliacoes.Add(avaliacao);
    }

    public List<Avaliacao> ObterAvaliacoes()
    {
        return _avaliacoes;
    }

    public decimal ObterMedia()
    {
        if (_avaliacoes.Count == 0)
            return 0;

        decimal total = 0;
        foreach (var avaliacao in _avaliacoes)
            total += avaliacao.Nota;

        return total / _avaliacoes.Count;
    }

    public override void FazerAniversario()
    {
        throw new NotImplementedException();
    }
}