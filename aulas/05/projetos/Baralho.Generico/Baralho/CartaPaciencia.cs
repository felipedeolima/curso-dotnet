public class CartaPaciencia : Carta<Paciencia>
{
    public ENipe Nipe { get; set; }
    public CartaPaciencia(string nome, ENipe nipe) : base(new Paciencia(), nome)
    {
        Nipe = nipe;
    }

    public override string ToString()
    {
        return $"{Nome}, {Nipe.ToString()} de {Tipo}";
    }
}
