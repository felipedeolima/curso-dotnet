﻿// Projete classes abstratas para representar um jogo de cartas, como Carta e Baralho.
// Utilize generics para permitir a criação de diferentes tipos de cartas e baralhos (por exemplo, cartas de baralho francês ou baralho de poker).

var carta1 = new CartaUno("1", "vermelho");
var carta2 = new CartaUno("2", "vermelho");
var carta3 = new CartaUno("3", "vermelho");

var cartaP1 = new CartaPaciencia("1", ENipe.Copas);
var cartaP2 = new CartaPaciencia("2", ENipe.Copas);
var cartaP3 = new CartaPaciencia("3", ENipe.Copas);

var baralho = new BaralhoUno();
var baralhoP = new BaralhoPaciencia();

baralho.Cartas.Add(carta1);
baralho.Cartas.Add(carta2);
baralho.Cartas.Add(carta3);

baralhoP.Cartas.Add(cartaP1);
baralhoP.Cartas.Add(cartaP2);
baralhoP.Cartas.Add(cartaP3);

baralho.Embaralhar();
baralhoP.Embaralhar();

foreach (var carta in baralho.Cartas)
{
    Console.WriteLine(carta);
}

foreach (var carta in baralhoP.Cartas)
{
    Console.WriteLine(carta);
}
