# Aula 03

![Untitled](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPlcnhXDRWCEC0bkx6ZRia2lpyTk0FSx_W8g&usqp=CAU)

💡 Revisão de programação orientada a objetos básica com C#. Conceitos de Classe, instância, Métodos, Atributos, Construtor, encapsulamento, Herança, polimorfismo e interfaces.

## Sumário

- [x]  Conceitos de classe e objeto
- [x]  Atributo e Método
- [x]  Construtor
- [x]  Encapsulamento
- [x]  Herança e polimorfismo
- [x]  Exercícios

## Conteúdo

### Matéria

- **Classes** são estruturas que definem **objetos**. Elas contêm **atributos** (variáveis) e **métodos** (funções) que descrevem o comportamento do objeto. Um **objeto** é uma instância de uma classe.
- O **encapsulamento** é um conceito importante em programação orientada a objetos que permite ocultar o estado interno de um objeto e expor apenas uma interface pública para interagir com ele. Isso ajuda a garantir que o objeto seja usado corretamente e evita que o código externo cause danos ao objeto.
- A **herança** é um mecanismo que permite criar novas classes a partir de classes existentes. A nova classe herda os atributos e métodos da classe pai e pode adicionar novos atributos e métodos ou substituir os existentes.
- O **polimorfismo** é a capacidade de um objeto ser tratado como se fosse outro objeto. Isso é útil quando você tem vários objetos que compartilham um comportamento comum, mas têm implementações diferentes.

Aqui está um exemplo de código que demonstra esses conceitos:

```csharp
using System;

class Animal
{
    public string Name { get; set; }
    public int Age { get; set; }

    public virtual void MakeSound()
    {
        Console.WriteLine("The animal makes a sound");
    }
}

class Dog : Animal
{
    public override void MakeSound()
    {
        Console.WriteLine("The dog barks");
    }
}

class Cat : Animal
{
    public override void MakeSound()
    {
        Console.WriteLine("The cat meows");
    }
}

class Program
{
    static void Main(string[] args)
    {
        Animal animal = new Animal();
        animal.MakeSound();

        Dog dog = new Dog();
        dog.MakeSound();

        Cat cat = new Cat();
        cat.MakeSound();
    }
}

```

Este código define uma classe `Animal` com dois atributos (`Name` e `Age`) e um método virtual `MakeSound`. As classes `Dog` e `Cat` herdam da classe `Animal` e substituem o método `MakeSound` para produzir o som correto. O método `Main` cria instâncias de cada classe e chama o método `MakeSound` para cada uma.

[Tutorial: criar uma ferramenta .NET - .NET CLI | Microsoft Learn](https://learn.microsoft.com/pt-br/dotnet/core/tools/global-tools-how-to-create)

## Exercícios

**Nível Iniciante:**

1. **Calculadora Simples:**
    - Crie uma classe **`Calculadora`** com métodos para adição, subtração, multiplicação e divisão. Utilize encapsulamento para proteger os dados.
2. **Cadastro de Alunos:**
    - Desenvolva um sistema simples de cadastro de alunos com as classes **`Aluno`** e **`Turma`**. Permita adicionar alunos à turma e calcular a média da turma.

**Nível Intermediário:**

1. **Jogo de Cartas:**
    - Implemente um jogo de cartas com as classes **`Baralho`**, **`Carta`** e **`Jogador`**. Adicione funcionalidades como embaralhar o baralho e distribuir cartas aos jogadores.
2. **Sistema Bancário Avançado:**
    - Aprimore o sistema bancário anterior, adicionando suporte a transferências entre contas, histórico de transações e tratamento de exceções para operações inválidas.
