﻿void PrintArea(Shape shape)
{
    Console.WriteLine($"Area: {shape.Area()}");
}


Rectangle rectangle = new Rectangle { Width = 5, Height = 4 };
Square square = new Square { SideLength = 5 };
Circle circle = new Circle { Radius = 2 };

PrintArea(rectangle); // Output: Area: 20
PrintArea(square);   // Output: Area: 25
PrintArea(circle);   // Out put: Area: 12.5663706143592

string nomeDaClasse = typeof(Rectangle).Name;
Console.WriteLine($"O nome da classe Rectangle é: {nomeDaClasse}");
